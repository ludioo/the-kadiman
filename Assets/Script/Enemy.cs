﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update
    GameManager gm;
    public Sprite[] sprites;
    public bool pocong, kuntilanak, tuyul;
    public DragableObject dragableObject;
    private Button enemy;
    public Animator animator;
    public GameObject profilPanel;
    public CameraShaking cameraShaking;
    //public int jumlahObject;

    Image image;
    void Start()
    {
        gm = GameObject.Find("GameManager").GetComponent<GameManager>();
        image = GetComponent<Image>();
        GetComponent<Button>().onClick.AddListener(ShowProfile);

    }

    public void ChangeSprite()
    {
        if (image.sprite == sprites[1])
        {
            image.sprite = sprites[2];
        }
        else
        {
            image.sprite = sprites[1];
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
            if(pocong && !tuyul && !kuntilanak)
            {
                if(collision.gameObject.tag == "pocongObject" && collision is PolygonCollider2D)
                {
                    Destroy(collision.gameObject);
                    gm.setanPutihCurrent++;
                    gm.UpdateText();
                    ChangeSprite();
                    animator.SetBool("Done", true);
                    profilPanel.SetActive(false);
                    //return;
                    Debug.Log("Sukses Pocong");


                }
                else if (collision.gameObject.tag != "pocongObject" && collision is PolygonCollider2D && collision.gameObject.tag != "kadiman")
                {
                    gm.setanPutihMax--;
                    gm.textUI[0].color = Color.red;
                    gm.UpdateText();
                    cameraShaking.ShakeIt();
                    
                }
            }
            if(tuyul && !pocong && !kuntilanak)
            {
                if (collision.gameObject.tag == "tuyulObject" && collision is PolygonCollider2D)
                {
                    Destroy(collision.gameObject);
                    gm.setanPutihCurrent++;
                    gm.UpdateText();
                    ChangeSprite();
                    //animator.SetBool("Done", true);
                    //return;
                    Debug.Log("Sukses Tuyul");

                }
                else if (collision.gameObject.tag != "tuyulObject" && collision.gameObject.tag != "kadiman" && collision is PolygonCollider2D)
                {
                    gm.setanPutihMax--;
                    gm.textUI[0].color = Color.red;
                    gm.UpdateText();
                    cameraShaking.ShakeIt();
                }
            }
    }

    void ShowProfile()
    {
        profilPanel.SetActive(true);
        Debug.Log("ProfileShows");
    }
}
