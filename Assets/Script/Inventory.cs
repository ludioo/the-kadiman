﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public bool[] isFull;
    public GameObject[] slots;
    public GameObject[] dropButton;
    public GameObject inv;
    public bool isInvOpen = false;
    GameObject panelInv;
    GameObject panelInvClose;
    public GameObject canvas;
    // Start is called before the first frame update
    void Start()
    {
        panelInv = GameObject.Find("panelInvOpen");
        panelInv.SetActive(false);
        panelInvClose = GameObject.Find("panelInvClose");
        inv.SetActive(false);
    }

    private void Awake()
    {

    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void OpenInv()
    {
        if (inv != null && isInvOpen == false)
        {
            bool isActive = inv.activeSelf;
            inv.SetActive(!isActive);
            isInvOpen = true;
            panelInv.SetActive(true);
            panelInvClose.SetActive(false);
        }
        else if(inv != null && isInvOpen == true)
        {
            bool isActive = inv.activeSelf;
            inv.SetActive(!isActive);
            isInvOpen = false;
            panelInv.SetActive(false);
            panelInvClose.SetActive(true);
        }
    }
}
