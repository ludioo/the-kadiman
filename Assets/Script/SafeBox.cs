﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeBox : MonoBehaviour
{
    public GameObject panel;
    SpriteRenderer kadiman;
    public bool destroyed;
    // Start is called before the first frame update
    void Start()
    {
        kadiman = GameObject.Find("kadiman").GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "kadiman" && Input.GetKeyDown(KeyCode.Z) && !destroyed)
        {
            panel.SetActive(true);
            kadiman.sortingOrder = -1;
        }
        else if (destroyed)
        {

        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag == "kadiman")
        {
            panel.SetActive(false);
            kadiman.sortingOrder = 1;
        }
    }

    public void ClosePanel()
    {
        panel.SetActive(false);
        kadiman.sortingOrder = 1;
    }
}
