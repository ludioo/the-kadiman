﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    //public GameObject[] panelProfil;
    //public Animator[] animatorProfil;
    public Text[] textUI;
    public int setanPutihMax, setanPutihCurrent, setanMerahMax, setanMerahCurrent;

    // Start is called before the first frame update
    void Start()
    {
        UpdateText();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void UpdateText()
    {
        textUI[0].text = "/" + setanPutihMax.ToString();
        textUI[1].text = "/" + setanMerahMax.ToString();
        textUI[2].text = setanPutihCurrent.ToString();
        textUI[3].text = setanMerahCurrent.ToString();

        if (setanPutihMax < 1)
        {
            setanPutihMax = 1;
        }
        else if (setanMerahMax <= 0)
        {
            setanMerahMax = 0;
        }
        else if (setanPutihCurrent >= 2)
        {
            setanPutihCurrent = 2;
        }
        else if (setanMerahCurrent >= 1)
        {
            setanMerahCurrent = 1;
        }
    }
}
