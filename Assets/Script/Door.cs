﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Door : MonoBehaviour
{
    public string sceneName;

    public GameObject arrowIn;

    private void Start()
    {
        arrowIn.gameObject.SetActive(false);
        arrowIn.GetComponent<Button>().onClick.AddListener(ChangeScene);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "kadiman")
        {
            arrowIn.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "kadiman")
        {
            arrowIn.gameObject.SetActive(false);
        }
    }
    public void ChangeScene()
    {
        SceneManager.LoadScene(sceneName);
        GameObject.FindGameObjectWithTag("CanvasSetting").GetComponent<CanvasSetting>().setCamera();
    }
}
