﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragableObject : MonoBehaviour
{
    private float startPosX;
    private float startPosY;
    public bool isBeingHeld = false;
    [SerializeField]
    public bool kuntilanakObject, pocongObject, tuyulObject;
    public Enemy[] enemies;
    public Animator[] enemiesAnimator;
    GameManager gameManager;
    private void Start()
    {
        //enemies[0] = GameObject.FindGameObjectWithTag("pocong").GetComponent<Enemy>();
        //enemies[1] = GameObject.FindGameObjectWithTag("kuntilanak").GetComponent<Enemy>();
        //enemiesAnimator[0] = GameObject.FindGameObjectWithTag("pocong").GetComponent<Animator>();
        //enemiesAnimator[1] = GameObject.FindGameObjectWithTag("kuntilanak").GetComponent<Animator>();
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        
    }
    void Update()
    {
        if (isBeingHeld == true)
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);
            this.gameObject.transform.localPosition = new Vector3(mousePos.x - startPosX, mousePos.y - startPosY, 0);
        }
    }

    private void OnMouseDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mousePos;
            mousePos = Input.mousePosition;
            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            startPosX = mousePos.x - this.transform.localPosition.x;
            startPosY = mousePos.y - this.transform.localPosition.y;

            isBeingHeld = true;

        }
    }

    private void OnMouseUp()
    {
        isBeingHeld = false;
        Debug.Log("lepas");
    }

    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.gameObject.tag == "kuntilanak")
    //    {
    //        if (!kuntilanakObject)
    //        {
    //            if (!isBeingHeld)
    //            {
    //                //Destroy(this.gameObject);
    //                //enemies[1].jumlahObject++;
    //                //if (enemies[1].jumlahObject == 3)
    //                //{
    //                gameManager.setanMerahMax--;
    //                gameManager.textUI[1].color = Color.red;
    //                gameManager.UpdateText();
    //                ///enemies[1].GetComponent<SpriteRenderer>().sortingOrder = -20;
    //                return;
    //            }

    //        }
    //        else if (kuntilanakObject)
    //        {
    //            if (!isBeingHeld)
    //            {
    //                Destroy(this.gameObject);
    //                gameManager.UpdateText();
    //                //enemies[1].jumlahObject--;
    //                //if (enemies[1].jumlahObject == 0)
    //                //{
    //                //enemies[1].ChangeSprite();
    //                //    enemiesAnimator[1].SetBool("Done", true);
    //                //}
    //                return;
    //            }
    //        }
    //    }



    //    if (collision.gameObject.tag == "pocong")
    //    {
    //        if (pocongObject)
    //        {
    //            if (!isBeingHeld)
    //            {
    //                Destroy(this.gameObject);
    //                //enemies[0].jumlahObject--;
    //                //if (enemies[0].jumlahObject == 0)
    //                //{
    //                //enemies[0].ChangeSprite();
    //                //enemiesAnimator[0].SetBool("Done", true);
    //                Debug.Log($"nambah cok {gameManager.setanPutihCurrent}");
    //                gameManager.setanPutihCurrent++;
    //                gameManager.UpdateText();
    //                return;
    //            }

    //        }
    //        else if(!pocongObject)
    //        {
    //            if (!isBeingHeld)
    //            {
    //                //Destroy(this.gameObject);
    //                //enemies[0].jumlahObject++;
    //                //if (enemies[0].jumlahObject == 3)
    //                //{
    //                gameManager.setanPutihMax--;
    //                gameManager.textUI[0].color = Color.red;
    //                gameManager.UpdateText();

    //                return;
    //                //  enemies[0].GetComponent<SpriteRenderer>().sortingOrder = -20;
    //            }
    //        }
    //    }
    //}
}