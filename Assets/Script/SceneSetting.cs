﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSetting : MonoBehaviour
{
    Scene scene;
    string sceneName;
    GameObject kadiman;

    private void Start()
    {
        scene = SceneManager.GetActiveScene();
        sceneName = scene.name;
        kadiman = GameObject.Find("kadiman");
        if (sceneName == "Outside")
        {
            kadiman.transform.position = new Vector3(5f, 0f, 0f);
            kadiman.transform.localScale = new Vector3(25f, 25f, 0);
            Debug.Log("Work");
        }
        else if (sceneName == "Bakery")
        {
            kadiman.transform.position = new Vector3(-4.52f, -1.42f, 0f);
            kadiman.transform.localScale = new Vector3(35f, 35f, 0f);
        }
        else if (sceneName == "KadimanHouse")
        {
            kadiman.transform.position = new Vector3(-5.79f, -1.65f, 0f);
            kadiman.transform.localScale = new Vector3(35f, 35f, 0f);
        }
        else if (sceneName == "Police")
        {
            kadiman.transform.position = new Vector3(7.49f, 0f, 0);
            kadiman.transform.localScale = new Vector3(15f, 15f, 0);
        }
    }

}
