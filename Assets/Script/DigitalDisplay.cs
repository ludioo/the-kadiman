﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DigitalDisplay : MonoBehaviour
{
    [SerializeField]
    private Sprite[] digits;

    [SerializeField]
    private Image[] characters;

    private string codeSequence;
    public GameObject uangGold;
    public Transform safeBoxPos;
    SafeBox safeBox;

    public CameraShaking cameraShaking;
    // Start is called before the first frame update

    void Start()
    {
        safeBox = GameObject.Find("safeBox").GetComponent<SafeBox>();
        codeSequence = "";
        for(int i = 0; i <= characters.Length-1;i++)
        {
            characters[i].sprite = digits[10];
        }
        PushButton.ButtonPressed += AddDigitsToCodeSequence;
    }

    private void AddDigitsToCodeSequence(string digitEntered)
    {
        if(codeSequence.Length < 4)
        {
            switch(digitEntered)
            {
                case "Zero":
                    codeSequence += "0";
                    DisplayCodeSequence(0);
                    break;
                case "One":
                    codeSequence += "1";
                    DisplayCodeSequence(1);
                    break;
                case "Two":
                    codeSequence += "2";
                    DisplayCodeSequence(2);
                    break;
                case "Three":
                    codeSequence += "3";
                    DisplayCodeSequence(3);
                    break;
                case "Four":
                    codeSequence += "4";
                    DisplayCodeSequence(4);
                    break;
                case "Five":
                    codeSequence += "5";
                    DisplayCodeSequence(5);
                    break;
                case "Six":
                    codeSequence += "6";
                    DisplayCodeSequence(6);
                    break;
                case "Seven":
                    codeSequence += "7";
                    DisplayCodeSequence(7);
                    break;
                case "Eight":
                    codeSequence += "8";
                    DisplayCodeSequence(8);
                    break;
                case "Nine":
                    codeSequence += "9";
                    DisplayCodeSequence(9);
                    break;

            }
        }
        switch(digitEntered)
        {
            case "Clear":
                ResetDisplay();
                break;

            case "OK":
                if(codeSequence.Length > 0)
                {
                    CheckResult();
                }
                break;
        }
    }

    private void DisplayCodeSequence(int digitJustEntered)
    {
        switch(codeSequence.Length)
        {
            case 1:
                characters[0].sprite = digits[10];
                characters[1].sprite = digits[10];
                characters[2].sprite = digits[10];
                characters[3].sprite = digits[digitJustEntered];
                break;
            case 2:
                characters[0].sprite = digits[10];
                characters[1].sprite = digits[10];
                characters[2].sprite = characters[3].sprite;
                characters[3].sprite = digits[digitJustEntered];
                break;
            case 3:
                characters[0].sprite = digits[10];
                characters[1].sprite = characters[2].sprite;
                characters[2].sprite = characters[3].sprite;
                characters[3].sprite = digits[digitJustEntered];
                break;
            case 4:
                characters[0].sprite = characters[1].sprite;
                characters[1].sprite = characters[2].sprite;
                characters[2].sprite = characters[3].sprite;
                characters[3].sprite = digits[digitJustEntered];
                break;

        }
    }

    private void CheckResult()
    {
        if(codeSequence == "3750")
        {
            Debug.Log("Correct!");
            Instantiate(uangGold,safeBoxPos);
            this.transform.parent.gameObject.SetActive(false);
            safeBox.destroyed = true;
            safeBox.panel.SetActive(false);
        }
        else
        {
            Debug.Log("Wrong!");
            cameraShaking.ShakeIt();

        }
    }

    private void ResetDisplay()
    {
        for(int i = 0; i <= characters.Length -1; i++)
        {
            characters[i].sprite = digits[10];
        }
        codeSequence = "";
    }

    private void OnDestroy()
    {
        PushButton.ButtonPressed -= AddDigitsToCodeSequence;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
