﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inputfiel_Script : MonoBehaviour
{
    public GameObject spawner;
    public GameObject[] allObject;
    Enemy enemy;
    public Animator[] enemiesAnimator;
    // Start is called before the first frame update
    void Start()
    {
        enemy = GameObject.Find("kuntilanak").GetComponent<Enemy>();
        var input = gameObject.GetComponent<InputField>();
        var acc = new InputField.SubmitEvent();
        acc.AddListener(Create);
        acc.AddListener(Adjective);
        input.onEndEdit = acc;
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void Create(string word)
    {
        if (word == "jersey" || word == "baju bola" || word == "baju sepak bola")
        {
            Instantiate(allObject[0], spawner.transform.position, spawner.transform.rotation);
        }
        else if (word == "bola" || word == "bola sepak")
        {
            Instantiate(allObject[1], spawner.transform.position, spawner.transform.rotation);
        }
        else if (word == "sepatu bola" || word == "sepatu sepak bola")
        {
            Instantiate(allObject[2], spawner.transform.position, spawner.transform.rotation);
        }
        else if (word == "uang")
        {
            Instantiate(allObject[3], spawner.transform.position, spawner.transform.rotation);
        }
        else if (word == "dress" || word == "baju baru")
        {
            Instantiate(allObject[4], spawner.transform.position, spawner.transform.rotation);
        }
        else
        {
            Debug.Log("Not found");
        }
    }

    public void Adjective(string word)
    {
        if (word == "cantik")
        {
            enemy.ChangeSprite();
            enemiesAnimator[1].SetBool("Done", true);
        }
    }
}
