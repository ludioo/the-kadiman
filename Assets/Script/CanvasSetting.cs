﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasSetting : MonoBehaviour
{
    private Canvas canvas;
    new Camera camera;
    // Start is called before the first frame update
    void Awake()
    {
        setCamera();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void setCamera()
    {
        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        canvas = GameObject.FindGameObjectWithTag("Canvas").GetComponent<Canvas>();

        if (canvas != null)
        {
            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            canvas.worldCamera = camera;
        }
    }
}
